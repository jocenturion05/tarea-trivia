//
//  ViewController.swift
//  tarea-trivia
//
//  Created by Bootcamp 4 on 2022-10-27.
//
import UIKit
class ViewController: UIViewController {
    @IBOutlet weak var buttonTru: UIButton!
    @IBOutlet weak var buttonFals: UIButton!
    @IBOutlet weak var labelQuest: UILabel!
    @IBOutlet weak var labelCase: UILabel!
    let question = ["1 + 1 = 2?", "2 + 2 = 5?"]
    let answers  = [1,2]
    var index = 0
    var num = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        buttonTru.tintColor  = UIColor.brown
        buttonFals.tintColor = UIColor.darkGray
        labelCase.text = " "
        matchDisplayFirst()
    }
    func matchDisplayFirst(){
        labelQuest.text = question[0]
    }
    func matchDisplayCurrent(){
        labelQuest.text = question[index]
    }
    @IBAction func Tap(_ sender: UIButton) {
        if(sender.isEqual(buttonTru)){
            num = 1
        }
        if(sender.isEqual(buttonFals)){
            num = 2
        }
        checkTrue()
    }
    func checkTrue(){
            if(answers[index] == num){
                labelCase.text = "True"
            }
            else{
                labelCase.text = "False"
            }
            if(index <= answers.count - 1){
                matchDisplayCurrent()
            }
    }
    @IBAction func Button(_ sender: Any) {
        index = index + 1
        labelCase.text = " "
        if(index <= answers.count - 1){
            matchDisplayCurrent()
        }
        else{
            labelQuest.text = "No more questions"
        }
    }
}

